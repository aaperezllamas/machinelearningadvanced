#!/usr/bin/env python
# coding: utf-8

# In[5]:


import numpy as np
import matplotlib.pyplot as plt
import copy
get_ipython().run_line_magic('matplotlib', 'inline')


# In[6]:


class simpleMDP():
    
    def __init__(self, mu=-0.1, std=1, max_actions=10):
        
        self.mu = mu
        self.std = std
        self.max_actions = max_actions
        
        # Action numbers
        self.right, self.left = 0, 1
        # Define actions available for each state
        self.state_actions = {
            'A': [self.right, self.left],
            'B': [i for i in range(max_actions)],
            'C': [self.right], 
            'D': [self.left] }

        self.state_transitions = {
            'A': {self.right: 'C',
                  self.left: 'B'},
            'B': {a: 'D' for a in range(max_actions)},
            'C': {self.right: 'Done'},
            'D': {self.left: 'Done'}
        }
        
        self.state = 'A'
    
    def step(self, action):
        self.state = self.state_transitions[self.state][action]
        # reward = 0 for all transitions except from B to D
        reward = np.random.normal(self.mu, self.std) if self.state == 'D' else 0
        done = True if self.state == 'D' or self.state == 'C' else False
        return self.state, reward, done, None
    
    def available_actions(self, state=None):
        if state is None:
            return self.state_actions[self.state]
        else:
            return self.state_actions[state]
    
    def sample_actions(self):
        return np.random.choice(self.available_actions())
    
    def reset(self):
        self.state = 'A'
        return self.state


# In[7]:


# Q-Learning for Sutton Example
env = simpleMDP()

max_tests = 10000
n_eps = 300
eps = 0.1
lr = 0.1

left_count_q = np.zeros(n_eps)
q_estimate = np.zeros(n_eps)
t = 0
s_1 = None
while t < max_tests:
    Q = {state: np.zeros(env.max_actions) for state in env.state_actions.keys()}
    for ep in range(n_eps):
        s_0 = env.reset()
        while True:
            # Select eps-greedy action
            if np.random.uniform() < eps:
                action = env.sample_actions()
            else:
                # Break ties among max values randomly if ties exist
                # If no ties exist, the max will be selected with prob=1
                max_qs = np.where(
                    np.max(Q[s_0][env.available_actions()])==
                        Q[s_0][env.available_actions()])[0]
                action = np.random.choice(max_qs)
                
            # Count left actions from A
            if s_0 == 'A' and action == 1:
                left_count_q[ep] += 1

            s_1, reward, done, _ = env.step(action)

            # Update Q-Tables
            Q[s_0][action] += lr * (reward + np.max(Q[s_1][env.available_actions()]) - 
                                    Q[s_0][action])
            s_0 = s_1
            if done:
                q_estimate[ep] += (Q['A'][env.left] - q_estimate[ep]) / (ep + 1)
                break
    t += 1
   # print(t)
    


# In[9]:


# Double Q-Learning
env = simpleMDP()

max_tests = 10000
n_eps = 300
eps = 0.1
lr = 0.1

left_count_dq = np.zeros(n_eps)
q1_estimate = np.zeros(n_eps)
q2_estimate = np.zeros(n_eps)
t = 0
s_1 = None
while t < max_tests:
    Q1 = {state: np.zeros(env.max_actions) for state in env.state_actions.keys()}
    Q2 = copy.deepcopy(Q1)
    for ep in range(n_eps):
        s_0 = env.reset()
        while True:
            # Select eps-greedy action
            if np.random.uniform() < eps or ep == 0:
                action = env.sample_actions()
            else:
                # If no ties exist, the max will be selected with prob=1
                Q_sum = Q1[s_0][env.available_actions()] +                         Q2[s_0][env.available_actions()]
                max_qs = np.where(np.max(Q_sum)==Q_sum)[0]
                action = np.random.choice(max_qs)
                
            # Count left actions from A
            if s_0 == 'A' and action == 1:
                left_count_dq[ep] += 1

            s_1, reward, done, _ = env.step(action)

            # Update Q-Tables
            if np.random.uniform() < 0.5:
                Q1[s_0][action] += lr * (reward +                     Q2[s_1][np.argmax(Q1[s_1][env.state_actions[s_1]])] 
                                         - Q1[s_0][action])
            else:
                Q2[s_0][action] += lr * (reward +                     Q1[s_1][np.argmax(Q2[s_1][env.state_actions[s_1]])]                                          - Q2[s_0][action])
            s_0 = s_1
            if done:
                q1_estimate[ep] += (Q1['A'][env.left] - q1_estimate[ep]) / (ep + 1)
                q2_estimate[ep] += (Q2['A'][env.left] - q2_estimate[ep]) / (ep + 1)
                break
    t += 1
print(t)


# In[10]:


plt.figure(figsize=(15,8))
plt.plot(left_count_q/max_tests*100, 
         label='Q-Learning')
plt.plot(left_count_dq/max_tests*100, 
         label='Double Q-Learning')
plt.hlines(xmin=0, xmax=n_eps, y=eps/len(env.state_actions.keys())*100, label='Optimal')
plt.ylabel('Percentage of Left Actions')
plt.xlabel('Episodes')
plt.title(r'Q-Learning Action Selection ($\epsilon=0.1$)')
plt.legend(loc='best')
plt.show()


# In[11]:


mean_q_estimate = (q1_estimate + q2_estimate) / 2
plt.figure(figsize=(12,8))
plt.plot(q_estimate, label='Q-Learning')
plt.plot(mean_q_estimate, label='Double Q-Learning')
plt.legend()
plt.xlabel('Episode')
plt.ylabel('Estimated Value')
plt.title('Estimated Value of Choosing Left from State A')
plt.show()


# In[ ]:




